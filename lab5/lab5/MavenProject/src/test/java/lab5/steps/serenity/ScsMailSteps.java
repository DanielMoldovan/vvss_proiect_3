package lab5.steps.serenity;

import lab5.pages.ScsAddressBookPage;
import lab5.pages.ScsLoginPage;
import lab5.pages.ScsMainViewPage;
import net.thucydides.core.annotations.Step;

public class ScsMailSteps {

    private ScsLoginPage scsLoginPage;
    private ScsMainViewPage scsMainViewPage;
    private ScsAddressBookPage scsAddressBookPage;

    @Step
    public void do_login(String username, String password) {
        scsLoginPage.open();
        scsLoginPage.do_login(username, password);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step
    public void check_invalid_login(String value) {
        assert scsLoginPage.return_message_from_message_div("warning").equals(value);
    }

    @Step
    public void check_valid_login(String value) {
        scsMainViewPage.do_logout();
        assert scsLoginPage.return_message_from_message_div("notice").equals(value);
    }

    @Step
    public void do_search(String search_text) {
        scsMainViewPage.do_search(search_text);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step
    public void check_valid_search(String value) {
        assert scsMainViewPage.return_message_from_message_div("confirmation").equals(value);
    }

    @Step
    public void check_invalid_search(String value) {
        assert scsMainViewPage.return_message_from_message_div("notice").equals(value);
    }

    @Step
    public void add_valid_new_contact(String firstName, String lastName, String email) {
        scsMainViewPage.click_address_book();
        scsAddressBookPage.do_add_new_contact(firstName, lastName, email);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assert scsAddressBookPage.return_message_from_message_div("confirmation").equals("Successfully saved.");

        scsAddressBookPage.do_remove_contact(firstName, lastName);
    }

    @Step
    public void add_invalid_new_contact(String firstName, String lastName, String email) {
        scsMainViewPage.click_address_book();
        scsAddressBookPage.do_add_new_contact(firstName, lastName, email);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assert scsAddressBookPage.return_message_from_message_div("warning").contains("Invalid e-mail address: " + email);
    }
}