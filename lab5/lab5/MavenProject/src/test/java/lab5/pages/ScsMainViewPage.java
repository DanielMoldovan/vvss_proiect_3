package lab5.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class ScsMainViewPage extends PageObject {

    @FindBy(id="rcmbtn103")
    private WebElementFacade logoutButton;

    @FindBy(id="quicksearchbox")
    private WebElementFacade searchBox;

    @FindBy(id = "message")
    private WebElementFacade messageDiv;

    @FindBy(id = "rcmbtn101")
    private WebElementFacade addressBookButton;

    public void do_logout() {
        this.logoutButton.click();
    }

    public void do_search(String what_to_search_for) {
        this.searchBox.type(what_to_search_for);
        this.searchBox.submit();
    }

    public String return_message_from_message_div(String element) {
        return this.messageDiv.findElement(By.className(element)).getText();
    }

    public void click_address_book() {
        this.addressBookButton.click();
    }
}
