package lab5.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ScsAddressBookPage extends PageObject {

    @FindBy(id = "rcmbtn104")
    private WebElementFacade newContactButton;

    @FindBy(id = "message")
    private WebElementFacade messageDiv;

    public void do_add_new_contact(String firstName, String lastName, String email) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        newContactButton.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement contact_frame = getDriver().findElement(By.xpath("//iframe[@id='contact-frame']"));
        getDriver().switchTo().frame(contact_frame);

        getDriver().findElement(By.id("ff_firstname")).sendKeys(firstName);
        getDriver().findElement(By.id("ff_surname")).sendKeys(lastName);
        getDriver().findElement(By.name("_email[]")).sendKeys(email);
        getDriver().findElement(By.id("rcmbtn101")).click();

        getDriver().switchTo().defaultContent();
    }

    public void do_remove_contact(String firstName, String lastName) {
        WebElement contactToDelete = getDriver().findElement(By.id("addresslist")).findElement(By.className("boxlistcontent")).
                findElement(By.xpath("//td[text()=\"" + firstName + " " + lastName + "\"]"));

        contactToDelete.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        getDriver().findElement(By.id("rcmbtn106")).click();
        Alert alert = getDriver().switchTo().alert();
        alert.accept();
        getDriver().switchTo().defaultContent();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String return_message_from_message_div(String element) {
        return this.messageDiv.findElement(By.className(element)).getText();
    }
}
