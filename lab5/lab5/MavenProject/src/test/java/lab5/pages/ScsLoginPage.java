package lab5.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

@DefaultUrl("https://www.scs.ubbcluj.ro/webmail/")
public class ScsLoginPage extends PageObject {

    @FindBy(id = "rcmloginuser")
    private WebElementFacade username;

    @FindBy(id = "rcmloginpwd")
    private WebElementFacade password;

    @FindBy(id = "rcmloginsubmit")
    private WebElementFacade loginButton;

    @FindBy(id = "message")
    private WebElementFacade messageDiv;

    public void do_login(String username, String password) {
        this.username.type(username);
        this.password.type(password);
        this.loginButton.click();
    }

    public String return_message_from_message_div(String element) {
        return this.messageDiv.findElement(By.className(element)).getText();
    }
}
