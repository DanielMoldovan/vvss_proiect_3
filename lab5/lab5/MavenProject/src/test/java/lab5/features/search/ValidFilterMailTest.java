package lab5.features.search;

import lab5.steps.serenity.ScsMailSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class ValidFilterMailTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    private ScsMailSteps scsMailSteps;

    @Test
    public void do_valid_filter() {
        scsMailSteps.do_login("mdir2152", "Gw2baurom2");
        scsMailSteps.do_search("internship");
        scsMailSteps.check_valid_search("2 messages found.");
    }
}