package lab5.features.search;

import lab5.steps.serenity.ScsMailSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class InvalidFilterMailTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    private ScsMailSteps scsMailSteps;

    @Test
    public void do_invalid_filter() {
        scsMailSteps.do_login("mdir2152", "Gw2baurom2");
        scsMailSteps.do_search("ssssssssssssssaereadsdadsadadsadsas");
        scsMailSteps.check_invalid_search("Search returned no matches.");
    }
}