package lab5.features.search;

import lab5.steps.serenity.ScsMailSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class InvalidScsLoginTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    private ScsMailSteps scsMailSteps;

    @Test
    public void do_invalid_login() {
        scsMailSteps.do_login("username", "parola");
        scsMailSteps.check_invalid_login("Login failed.");
    }
}