package lab5.features.search;

import lab5.steps.serenity.ScsMailSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class ValidScsNewContactTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    private ScsMailSteps scsMailSteps;

    @Test
    public void do_valid_new_contact() {
        scsMailSteps.do_login("mdir2152", "Gw2baurom2");
        scsMailSteps.add_valid_new_contact("test", "test", "test@test.com");
    }
}