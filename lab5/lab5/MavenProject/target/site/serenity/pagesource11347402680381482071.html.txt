<html xmlns="http://www.w3.org/1999/xhtml" class=" js chrome webkit"><head>
<title>SCS Webmail :: Welcome to SCS Webmail</title>
<meta name="Robots" content="noindex,nofollow">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE">
<link rel="index" href="./?_task=login">
<link rel="shortcut icon" href="skins/classic/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="skins/classic/common.min.css?s=1510168456">

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="plugins/jqueryui/themes/classic/jquery-ui-1.9.2.custom.css?s=1510168455">
<script src="program/js/jquery.min.js?s=1510168456" type="text/javascript"></script>
<script src="program/js/common.min.js?s=1510168456" type="text/javascript"></script>
<script src="program/js/app.min.js?s=1510168456" type="text/javascript"></script>
<script src="program/js/jstz.min.js?s=1510168456" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */

var rcmail = new rcube_webmail();
rcmail.set_env({"task":"login","x_frame_options":"sameorigin","standard_windows":false,"cookie_domain":"","cookie_path":"\/","cookie_secure":true,"skin":"classic","refresh_interval":60,"session_lifetime":600,"action":"login","comm_path":".\/?_task=login","compose_extwin":false,"date_format":"dd\/mm\/yy","request_token":"60156d0c3a2cf3369a4391f63675fe96"});
rcmail.display_message("Login failed.","warning",0);
rcmail.gui_container("loginfooter","bottomline");
rcmail.add_label({"loading":"Loading...","servererror":"Server Error!","connerror":"Connection Error (Failed to reach the server)!","requesttimedout":"Request timed out","refreshing":"Refreshing..."});
rcmail.gui_object('message', 'message');
rcmail.gui_object('loginform', 'form');
/* ]]> */
</script>

<script type="text/javascript" src="plugins/jqueryui/js/jquery-ui-1.9.2.custom.min.js?s=1510168455"></script>
<script type="text/javascript" src="plugins/jqueryui/js/i18n/jquery.ui.datepicker-en-GB.js?s=1510168455"></script>
</head>
<body>

<img src="skins/classic/images/cs-logo.png" id="logo" border="0" style="margin:0 11px" alt="SCS Webmail">

<div id="message" style="display: block;"></div>

<div id="login-form">
<div class="boxtitle">Welcome to SCS Webmail</div>
<div class="boxcontent">

<form name="form" method="post" action="./?_task=login">
<input type="hidden" name="_token" value="60156d0c3a2cf3369a4391f63675fe96">
<input type="hidden" name="_task" value="login"><input type="hidden" name="_action" value="login"><input type="hidden" name="_timezone" id="rcmlogintz" value="Europe/Helsinki"><input type="hidden" name="_url" id="rcmloginurl" value="_task=login"><table summary="" border="0"><tbody><tr><td class="title"><label for="rcmloginuser">Username</label>
</td>
<td class="input"><input name="_user" id="rcmloginuser" required="required" autocapitalize="off" autocomplete="off" value="" type="text"></td>
</tr>
<tr><td class="title"><label for="rcmloginpwd">Password</label>
</td>
<td class="input"><input name="_pass" id="rcmloginpwd" required="required" autocapitalize="off" autocomplete="off" type="password"></td>
</tr>
</tbody>
</table>
<p class="formbuttons"><input type="submit" id="rcmloginsubmit" class="button mainaction" value="Login"></p>

</form>

</div>
</div>

<noscript>
  <p id="login-noscriptwarning">Warning: This webmail service requires Javascript! In order to use it please enable Javascript in your browser's settings.</p>
</noscript>

<div id="login-bottomline">
  SCS Webmail 
    
</div>



<script type="text/javascript">
/* <![CDATA[ */

$(document).ready(function(){ 
rcmail.init();
var images = ["skins\/classic\/images\/icons\/folders.png","skins\/classic\/images\/mail_footer.png","skins\/classic\/images\/taskicons.gif","skins\/classic\/images\/display\/loading.gif","skins\/classic\/images\/pagenav.gif","skins\/classic\/images\/mail_toolbar.png","skins\/classic\/images\/searchfield.gif","skins\/classic\/images\/messageicons.png","skins\/classic\/images\/icons\/reset.gif","skins\/classic\/images\/abook_toolbar.png","skins\/classic\/images\/icons\/groupactions.png","skins\/classic\/images\/watermark.gif"];
            for (var i=0; i<images.length; i++) {
                img = new Image();
                img.src = images[i];
            }
});
/* ]]> */
</script>


</body></html>