package agenda.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.repository.classes.RepositoryContactMock;
import agenda.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact contact;
	private RepositoryContact repositoryContact;
	
	@Before
	public void setUp() throws Exception {
		repositoryContact = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
		try {
			contact = new Contact("daniel",
					"adresa",
					"+40744176894");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		repositoryContact.addContact(contact);
		for(Contact c : repositoryContact.getContacts())
			if (c.equals(contact))
			{
				assertTrue(true);
				break;
			}
	}
	
	@Test
	public void testCase2()
	{
		try{
			repositoryContact.addContact((Contact) new Object());
		}
		catch(Exception e)
		{
			assertTrue(true);
		}	
	}
	
	@Test
	public void testCase3()
	{
		for(Contact c : repositoryContact.getContacts())
			repositoryContact.removeContact(c);
		
		try {
			contact = new Contact("daniel",
					"adresa",
					"+40744176894");
			repositoryContact.addContact(contact);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		if (repositoryContact.count() == 1)
			if (contact.equals(repositoryContact.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
		else assertTrue(false);
	}
	
}
