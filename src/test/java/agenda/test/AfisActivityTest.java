package agenda.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.interfaces.RepositoryActivity;
import agenda.repository.interfaces.RepositoryContact;

import org.junit.Before;
import org.junit.Test;

public class AfisActivityTest {

	RepositoryActivity repositoryActivity;

	@Before
	public void setUp() throws Exception {
		RepositoryContact repcon = new RepositoryContactFile();
		repositoryActivity = new RepositoryActivityFile(repcon);
	}

	@Test
	public void testCase1() {
		for (Activity act : repositoryActivity.getActivities())
			repositoryActivity.removeActivity(act);

		Calendar c = Calendar.getInstance();
		c.set(2018, 4, 01, 1, 00);
		Date start = c.getTime();

		c.set(2018, 4, 01, 1, 30);
		Date end = c.getTime();

		Activity act = new Activity("activity1", start, end,
				new LinkedList<Contact>(), "description1");

		repositoryActivity.addActivity(act);

		c.set(2018, 4, 01);
		List<Activity> result = repositoryActivity.activitiesOnDate("activity1", c.getTime());

		assertTrue(result.size() == 1);
	}

	@Test
	public void testCase2() {
		for (Activity act : repositoryActivity.getActivities())
			repositoryActivity.removeActivity(act);

		Calendar c = Calendar.getInstance();
        c.set(2018, 4, 01, 1, 00);
        Date start = c.getTime();
        c.set(2018, 4, 01, 1, 30);
        Date end = c.getTime();

        Activity act = new Activity("activity1", start, end,
                new LinkedList<Contact>(), "description1");


        repositoryActivity.addActivity(act);

		c.set(2018, 4, 01);
		try {
			repositoryActivity.activitiesOnDate(((Object) 1).toString(), c.getTime());
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase3() {
		for (Activity act : repositoryActivity.getActivities())
			repositoryActivity.removeActivity(act);

		try {
			repositoryActivity.activitiesOnDate("invalid date", (Date)(Object)"a");
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase4() {
		for (Activity act : repositoryActivity.getActivities())
			repositoryActivity.removeActivity(act);

		try {
			repositoryActivity.addActivity((Activity)(Object)1);
			
			Calendar c = Calendar.getInstance();
			c.set(2018, 4, 1);
			repositoryActivity.activitiesOnDate("activity1", c.getTime());
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase5() {
		for (Activity act : repositoryActivity.getActivities())
			repositoryActivity.removeActivity(act);
	
		Calendar c = Calendar.getInstance();
		c.set(2018, 4, 01);
		List<Activity> result = repositoryActivity.activitiesOnDate("orice", c.getTime());
		
		assertTrue(result.size() == 0);
	}
}
